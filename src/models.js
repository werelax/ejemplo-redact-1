var mori = require('mori');

var innerSet = function(route, state, field, value) {
  return mori.assocIn(state, route.concat(field), value);
};

var innerGet = function(route, state, field, notFound) {
  return mori.getIn(state, route.concat(field), notFound);
};

var innerUpdate = function(route, state, field, fn) {
  return mori.updateIn(state, route.concat(field), fn);
};

var accesor = function(route) {
  return {
    set: innerSet.bind(null, route),
    get: innerGet.bind(null, route),
    update: innerUpdate.bind(null, route),
    route: route
  };
};

module.exports = {
  ui: accesor(['ui']),
  data: accesor(['data'])
};
