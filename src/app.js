var state = require('./state_atom'),
    R$ = require('../lib/redact.js'),
    components = require('./components'),
    mori = require('mori'),
    // just to add them to the bundle.js
    controllers = require('./controllers');

window.onload = function() {

  state.swap(mori.toClj({
    ui: {toggled: false},
    data: {message: 'Oh, my...'}
  }));

  var update = R$.root(components.layout, document.body)(state.get());

  // better: debounce update
  state.addChangeListener(update);

};
