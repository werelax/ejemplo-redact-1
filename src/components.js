var d = require('../lib/redact').d,
    models = require('./models'),
    controllers = require('./controllers'),
    sender = require('./dispatcher').sender;

var banner = function(state) {
  var toggled = models.ui.get(state, 'toggled');
  return d('h1', null, toggled? "Yes!" : "Noo...");
};

var button = function(state) {
  // return d('button', {events: {click: controllers.button.onClick}},
  //          "Click Me!");
  return d('button', {events: {click: sender('button:click')} },
           "Click Me!");
};

var layout = function(state) {
  return d('div', {class: 'main'},
           d(banner, state),
           d(button, state));
};

module.exports = {
  layout: layout
};
