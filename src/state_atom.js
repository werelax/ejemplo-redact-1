var state;
var listeners = [];

module.exports = {
  get: function() {
    return state;
  },
  swap: function(newState) {
    state = newState;
    for (var i=listeners.length; i--;) listeners[i](state);
  },
  addChangeListener: function(fn) {
    listeners.push(fn);
  }
};
