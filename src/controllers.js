var state = require('./state_atom'),
    dispatcher = require('./dispatcher'),
    models = require('./models');

dispatcher.on('button:click', function() {
  var actual = state.get(),
      toggled = models.ui.get(actual, 'toggled');
  state.swap(models.ui.set(actual, 'toggled', !toggled));
});

module.exports = {
  button: {
    onClick: function() {
      var actual = state.get(),
          toggled = models.ui.get(actual, 'toggled');
      state.swap(models.ui.set(actual, 'toggled', !toggled));
    }
  }
};
